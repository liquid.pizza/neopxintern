# MQTT interface

The MQTT interface is **command based**.

## Topic

To send a command one has to publish on the topic `<basetopic>/cmd`.

e.g. `/led/cmd`

## Payload

The payload `msg` is an encoded JSON string.
After decoding and parsing `msg` (`ujson.loads(msg.decode())`) one has an dict object `msgObj`.

`msgObj` has at least two entries:

+ `"cmd"`: Contains the command
+ `"params"`: Contains additional parameters

`"params"` depends on `"cmd"`.

**Even if no `"params"` are needed, the payload has to include them!**

## Commands

Currently supported commands with an example payload.

All valid commands are defined in `cmdHandler.cmdMap`.

### ON

Switches from mode `STANDBY` to the last mode.

e.g.:

```json
{
    "cmd": "ON",
    "params": 0,
}
```

### OFF

Switches to mode `STANDBY`.
The current mode is saved for the [`ON`](#on) command.

e.g.:

```json
{
    "cmd": "OFF",
    "params": 0,
}
```

### TOGGLE

Toggles between the mode `STANDBY` and the last active mode.

e.g.:

```json
{
    "cmd": "TOGGLE",
    "params": 0,
}
```

### MODE

Set a new mode.

`params` is a dict.

The key `mode` is requiered.
It contains the new mode.

The key `config` is optional.
If it is set, the config for `mode` is updated with the new `config`.

e.g.:

```json
{
    "cmd": "MODE",
    "params": {
        "mode": "DISK",
        "lowLight": [
            0,
            0,
            5
        ],
        "highLight": [
            20,
            30,
            0
        ],
    }
}
```

### BRIGHTNESS

Set the brightness.
Brightness is set for all modes.

`params` is the new brightness value.
Only `int` values greater equal 0 and less equal 100 are accepted.

e.g.:

```json
{
    "cmd": "BRIGHTNESS",
    "params": 42
}
```

### SET

Set arbitrary options.

`params` contains a list of tuples.
Each of this tuples is structured the same way: `[<categorie>, <key>, <val>].

**This command can be destructive and should not be used!
Might be removed in future versions.**

e.g.:

```json
{
    "cmd": "SET",
    "params": [
        ["settings", "brightness", 50],
        ["setup", "loop", False],
    ]
}
```
