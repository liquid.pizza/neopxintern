# Fav Modes

Fav Modes

`{"cmd": "MODE", "params": {"mode": "GROW", "config": {"startCol": [100, 20, 0], "endCol": [100, 0, 20]}}}`

`{"cmd": "MODE", "params": {"mode": "DISK", "config": {"lowLight": [0, 3, 2], "highLight": [200, 0, 60], "shiftDelayInMs": 20}}}`

`{"cmd": "MODE", "params": {"mode": "DYNAMIC", "config": {"botList": [[100, 10, 0], [100, 0, 10]], "topList": [[90, 0, 30], [80, 20, 10]], "shiftDelayInMs": 100}}}`

`{"cmd": "MODE", "params": {"mode": "DYNAMIC", "config": {"botList": [[10, 50, 0], [5, 30, 10]], "topList": [[10, 20, 30], [0, 20, 60]], "shiftDelayInMs": 100}}}`
