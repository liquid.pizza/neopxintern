import dataHandler as dh

from dynamicMode import DynamicMode
from staticMode import StaticMode
from standbyMode import StandbyMode
from diskMode import DiskMode
from growMode import GrowMode
from percentMode import PercentMode
from timerMode import TimerMode

def setMode(mode):

    if (not mode in modeMap):
        return False

    modeMap[dh.ledConfig["settings"]["mode"]].exitMode()

    if dh.ledConfig["settings"]["mode"] != "STANDBY":
        # don't save STANDBY as last mode
        dh.lastMode = dh.ledConfig["settings"]["mode"]
        
    dh.ledConfig["settings"]["mode"] = mode

    modeMap[dh.ledConfig["settings"]["mode"]].enterMode()

    return True

def setConfig(mode, config):

    modeMap[mode].setConfig(config)

modeMap = {
    "STANDBY": StandbyMode(None),
    "STATIC": StaticMode(None),
    "DYNAMIC": DynamicMode(dh.ledConfig["modes"]["DYNAMIC"]),
    "DISK": DiskMode(dh.ledConfig["modes"]["DISK"]),
    "GROW": GrowMode(dh.ledConfig["modes"]["GROW"]),
    # "PERCENT": PercentMode(dh.ledConfig["modes"]["PERCENT"]),
    # "TIMER": TimerMode(dh.ledConfig["modes"]["TIMER"]),
}