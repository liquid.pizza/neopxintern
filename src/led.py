import time, neopixel, random, machine
import dataHandler as dh
from dataHandler import ledConfig as lc

np = neopixel.NeoPixel(machine.Pin(lc["setup"]["pinNum"]), lc["setup"]["numLeds"])

def getRndColor():
    return (random.getrandbits(8), random.getrandbits(8), random.getrandbits(8))

def getComplColor(c):
    return (255 - c[0], 255 - c[1], 255 - c[2])

def highlightLed(np, idx, hlCol = (255, 255, 255), llCol = (0, 0, 0)):
    for i in range(np.n):
        if (i == idx):
            c = hlCol
        else:
            c = llCol
        np[i] = c
    np.write()

def highlightAll(np, delayInMs, hlCol = (255, 255, 255), llCol = (0, 0, 0)):
    for i in range(np.n):
        highlightLed(np, i, hlCol, llCol)
        time.sleep_ms(delayInMs)

def dimmAll(np, start, end, step = 1, delayInMs = 800):
    for i in range(start, end, step):
        setAll(np, (i, i, i))
        time.sleep_ms(delayInMs)

    for i in range(end, start, step):
        setAll(np, (i, i, i))
        time.sleep_ms(delayInMs)

def glimmer(np):
    print("glimmer")

    incFactor = 1

    for i in range(np.n):
        print("###", i, "###")
        inc = random.getrandbits(1)
        r = random.getrandbits(2)

        print("inc:\t", inc, "\nr:\t", r)

        cntCol = np[i]
        print("cntCol:\t", cntCol)

        if (r == 3):
            continue

        if inc:
            if (r == 0):
                col = (cntCol[0] + incFactor, cntCol[1], cntCol[2])
            elif (r == 1):
                col = (cntCol[0], cntCol[1] + incFactor, cntCol[2])
            elif (r == 2):
                col = (cntCol[0], cntCol[1], cntCol[2] + incFactor)

        else:
            if (r == 0):
                col = (cntCol[0] - incFactor, cntCol[1], cntCol[2])
            elif (r == 1):
                col = (cntCol[0], cntCol[1] - incFactor, cntCol[2])
            elif (r == 2):
                col = (cntCol[0], cntCol[1], cntCol[2] - incFactor)

        print("new col:\t", col)

        np[i] = col

    np.write()

def stars(np, hlCol = (2, 3, 0), llCol = (0, 0, 0), bitNum = 10):
    for i in range(np.n):
        r = random.getrandbits(bitNum)

        if (not r):
            np[i] = hlCol
        else:
            np[i] = llCol
    np.write()

def starLoop(np, delayInMs):
    while(1):
        stars(np)
        time.sleep_ms(delayInMs)

def setAll(np, col):
    for i in range(np.n):
        np[i] = col
    np.write()

# alias for setAll(np, (0, 0, 0))
def off(np):
    setAll(np, (0, 0, 0))

#def setPixel(np, x, y, color):
def calcIdx(x, y):
    
    idx = 0

    sl = lc["setup"]["segmentLength"]

    # odd number check
    if x % 2:
        idx = x * sl + (sl - y) - 1
    # number is even
    else:
        idx = x * lc["setup"]["segmentLength"] + y
    
    return idx

def dimColor(rawColor):
    brightnessInPercent = lc["settings"]["brightness"] / 100
    return tuple(int(brightnessInPercent * c) for c in rawColor)

def recursiveDrop(np, height = None, hlCol = (255, 255, 255), llCol = (0, 0, 0), delayInMs = 100):

    if not height:
        height = np.n

    if (not height):
        return

    for h in range(height):
        for i in range(np.n):
            if i == h:
                np[height - 1 - i] = hlCol
            else:
                np[height - 1 - i] = llCol
        np.write()

    r, g, b = hlCol
    newHlColor = (r // 2, g // 2, b // 2)
    r, g, b = llCol
    newLlColor = (r // 2, g // 2, b // 2)
    newHeight = height // 2

    for h in range(newHeight):
        for i in range(np.n):
            if i == h:
                np[i] = newHlColor
            else:
                np[i] = newLlColor
        np.write()

    time.sleep_ms(delayInMs)

    recursiveDrop(np, newHeight, newHlColor, newLlColor, delayInMs // 2)