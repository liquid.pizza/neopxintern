class Mode:

    def __init__(self, config):
        print(type(self), "created")
        self._config = config
        self._run = False

    def __del__(self):
        print(type(self), "destroyed")

    def enterMode(self):
        self._run = True

    def loop(self):
        pass

    def exitMode(self):
        self._run = False

    def setConfig(self, config):
        self._config.update(config)