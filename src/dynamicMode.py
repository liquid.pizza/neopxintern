import led
import time
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class DynamicMode(Mode):

    def computeColElemByTime(self, t, stepNum, colStartElem, colEndElem):
        return (colEndElem - colStartElem) * t / stepNum + colStartElem

    def computeColByTime(self, t, stepNum, colStart, colEnd):
        startR, startG, startB = colStart
        endR, endG, endB = colEnd
        return (int(self.computeColElemByTime(t, stepNum, startR, endR)), int(self.computeColElemByTime(t, stepNum, startG, endG)), int(self.computeColElemByTime(t, stepNum, startB, endB)))

    def colorShiftByTime(self, np, colStart, colEnd, stepNum = 10, delayInMs = 100):
        for t in range(stepNum + 1):
            for i in range(np.n):
                np[i] = self.computeColByTime(t, stepNum, colStart, colEnd)
            np.write()
            time.sleep_ms(delayInMs)

    def computeColElemByIdx(self, i, num, colStartElem, colEndElem):
        return (colEndElem - colStartElem) * i / num + colStartElem

    def colorShiftByIdx(self, np, colStart, colEnd):
        for i in range(np.n):
            np[i] = self.computeColByIdx(i, np.n - 1, colStart, colEnd)
        np.write()

    def computeColByIdx(self, i, num, colStart, colEnd):
        startR, startG, startB = colStart
        endR, endG, endB = colEnd
        return (int(self.computeColElemByIdx(i, num, startR, endR)), int(self.computeColElemByIdx(i, num, startG, endG)), int(self.computeColElemByIdx(i, num, startB, endB)))

    def colorShiftByTimeAndIdx(self, np, colStartTop, colStartBot, colEndTop, colEndBot, stepNum, delayInMs = 100):
        print("Top:", colStartTop, "->", colEndTop)
        print("Bot:", colStartBot, "->", colEndBot)
        print("In", stepNum, "steps a", delayInMs, "ms")

        segmentLength = lc["setup"]["segmentLength"]
        numSegments = lc["setup"]["numSegments"]

        startCols = [self.computeColByIdx(segmentLength -1 - i, segmentLength - 1, colStartTop, colStartBot) for i in range(segmentLength)]
        endCols = [self.computeColByIdx(segmentLength -1 -i, segmentLength - 1, colEndTop, colEndBot) for i in range(segmentLength)]

        for t in range(stepNum):
            for i in range(segmentLength):

                rawColor = self.computeColByTime(t, stepNum - 1, startCols[i], endCols[i])
                col = led.dimColor(rawColor)

                for colNum in range(numSegments):
                    idx = 0
                    if colNum % 2:
                        idx = colNum * segmentLength + (segmentLength - i) - 1
                    else:
                        idx = colNum * segmentLength + i

                    if (not self._run) | dh.configIsUpdated:
                        return False

                    np[idx] = col
            np.write()
            time.sleep_ms(delayInMs)

        return True

    def enterMode(self, np = led.np):
        # reset indices
        self.topStartIdx = 0
        self.topEndIdx = min(1, len(self._config["topList"]) - 1)
        self.botStartIdx = 0
        self.botEndIdx = min(1, len(self._config["botList"]) - 1)
        self._run = True

    def exitMode(self):
        self._run = False

    def loop(self, np = led.np):
        # get colors
        colStartTop = self._config["topList"][self.topStartIdx]
        colEndTop = self._config["topList"][self.topEndIdx]
        colStartBot = self._config["botList"][self.botStartIdx]
        colEndBot = self._config["botList"][self.botEndIdx]

        ret = self.colorShiftByTimeAndIdx(np, colStartTop, colStartBot, colEndTop, colEndBot, self._config["stepNum"], self._config["shiftDelayInMs"])

        if not ret:
            return

        # safe the current end color indices as new start indices
        self.topStartIdx = self.topEndIdx
        self.botStartIdx = self.botEndIdx

        # increase start indices
        self.topEndIdx = (self.topStartIdx + 1) % len(self._config["topList"])
        self.botEndIdx = (self.botStartIdx + 1) % len(self._config["botList"])

        # sleep
        sleepTimeInMs = self._config["delayInMs"]
        print("Sleep for", sleepTimeInMs, "ms")
        for i in range(sleepTimeInMs // 10):
            time.sleep_ms(10)
            if dh.configIsUpdated:
                return
