import dataHandler as dh
import led
from modeHandler import modeMap
import modeHandler as mh

def handleAddColCmd(params):
    """Add a color to lists.
    Can take more than one list.

    Parameters:
    params (dict): Dict with the following entries:
        "colors" (list): List with colors as RGB value
        "lists" (list): Contains all names of lists the color should be added to
        "pos" (int): [Optional] Insert position. Default is 0

    e.g.
    ```
    params = {
        "colors": [[20, 30, 40], [90, 50, 20]],
        "lists": ["topList"],
        "pos": 2
    }
    ```
    """
    print("handleAddColCmd")
    lists = params["lists"]
    colors = params["colors"]
    pos = 0
    if "pos" in params:
        pos = params["pos"]

    for l in lists:
        if l in dh.ledConfig["colors"]:
            for c in colors:
                dh.ledConfig["colors"][l].insert(pos, c)
                print("Inserted", c, "at pos", pos)
        else:
            print("Invalid params!")
            return False
    return True

def handleClearColCmd(params):
    """Clear a color list.
    Can take more than one list.

    Parameters:
    params (dict): Dict with the following entries:
        "lists" (list): Contains all names of lists that should be cleared

    e.g.
    ```
    params = {
        "lists": ["topList"],
    }
    ```
    """
    print("handleClearColCmd")
    lists = params["lists"]
    for l in lists:
        dh.ledConfig["colors"][l].clear()
        print(dh.ledConfig["colors"][l])
    return True

def handleOffCmd(params):
    """Turn off the light.
    Set all LEDs to (0, 0, 0) and pause.

    Parameters:
    params (list): params can be empty

    e.g.
    ```
    params = {}
    ```
    """
    p = {"mode": "STANDBY"}
    return handleModeCmd(p)

def handleOnCmd(params):
    """Just unpauses the program.

    Parameters:
    params (list): params can be empty

    e.g.
    ```
    params = {}
    ```
    """
    if dh.ledConfig["settings"]["mode"] != "STANDBY":
        return False

    p = {"mode": dh.lastMode}
    return handleModeCmd(p)

def handleChangeSettingCmd(params):
    """Change arbitrary options.
    Can handle more than one option.

    Parameters:
    params (list): List with 3-tuples. Each is structured the same way:
        (categorie, key, value)

    e.g.
    ```
    params = {
        [
            ("setup", "loop", False),
            ("settings", "brightness", 50)
    }
    ```
    """

    print("handleChangeSettingsCmd")
    ret = False
    settings = params
    for s in settings:
        cat, key, val = s
        if cat in dh.ledConfig:
            if key in dh.ledConfig[cat]:
                dh.ledConfig[cat][key] = val
                print("Set", key, "in", cat, "to", val)
                ret = True
    return ret

def handlePauseCmd(params):
    p = {"mode": "STATIC"}
    return handleModeCmd(p)

def handleResumeCmd(params):
    p = {"mode": "DYNAMIC"}
    return handleModeCmd(p)

def handleModeCmd(params):
    print("handleModeCmd")
    m = params["mode"]

    if "config" in params:
        mh.setConfig(m, params["config"])

    return mh.setMode(m)

def handleToggleCmd(params):
    m = ""
    if dh.ledConfig["settings"]["mode"] == "STANDBY":
        m = dh.lastMode
    else:
        m = "STANDBY"
    
    return mh.setMode(m)

def handleBrightnessCmd(params):
    if isinstance(params, int):
        if 0 <= params <= 100:
            dh.ledConfig["settings"]["brightness"] = params
    return False

cmdMap = {
    "ON": handleOnCmd,
    "OFF": handleOffCmd,
    "TOGGLE": handleToggleCmd,
    "MODE": handleModeCmd,
    "SET": handleChangeSettingCmd,
    "BRIGHTNESS": handleBrightnessCmd,
}