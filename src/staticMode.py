import led
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class StaticMode(Mode):

    def enterMode(self, color = None):
        if color:
            led.setAll(led.np, color)

    def loop(self):
        pass