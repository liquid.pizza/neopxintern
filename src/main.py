import machine
import time
import random
import ujson
import ubinascii
import network
from umqtt.simple import MQTTClient
from machine import Pin
import _thread

import led
import dataHandler as dh
from dataHandler import ledConfig as lc
from cmdHandler import cmdMap

import modeHandler as mh

# Default MQTT server to connect to
SERVER = lc["connection"]["mqttServerIp"]
CLIENT_ID = ubinascii.hexlify(machine.unique_id())
TOPIC = lc["connection"]["baseTopicName"]

# color order:
#   Green
#   Red
#   Blue

def sub_cb(topic, msg):
    # `topic` and `msg` are byte arrays and have to be decoded (`decode()`)

    # split topic
    t = topic.decode().split("/")
    if t[-1] == "cmd":
        # convert the decoded JSON string to an object
        msgObj = ujson.loads(msg.decode())
        print(msgObj)
        try:
            cmd = msgObj["cmd"]
            params = msgObj["params"]
            dh.configIsUpdated = cmdMap[cmd](params)
        except KeyError:
            print("Missing key")


def mqttThread():
    print("Wifi enabled")

    station = network.WLAN(network.STA_IF)

    station.active(True)
    station.connect(lc["connection"]["wifiSsid"], lc["connection"]["wifiPassword"])

    while station.isconnected() == False:
        pass

    print('Connection successful')
    print(station.ifconfig())
    mqttClient = MQTTClient(CLIENT_ID, SERVER)

    # Subscribed messages will be delivered to this callback
    mqttClient.set_callback(sub_cb)
    mqttClient.connect()

    for t in [""] + lc["connection"]["lampGroups"]:
        time.sleep_ms(50)
        topic = TOPIC + t + "/+"
        mqttClient.subscribe(topic)
        print("Connected to %s, subscribed to %s topic" % (SERVER, topic))

    while 1:
        mqttClient.wait_msg()

def ledThread():

    mh.modeMap[lc["settings"]["mode"]].enterMode()

    while (1):
        if dh.configIsUpdated:
            dh.writeJson(lc)
        dh.configIsUpdated = False

        modeStr = lc["settings"]["mode"]
        
        mode = mh.modeMap[modeStr]
        if mode._run:
            mode.loop()

def main():
    print("Starting...")

    enableWifi = dh.ledConfig["setup"]["enableWifi"]
    print("Enable Wifi:", enableWifi)

    if (enableWifi):
        _thread.start_new_thread(mqttThread, ())

    print("Setup down.\nStart loop.")
    ledThread()


if __name__ == "__main__":
    main()