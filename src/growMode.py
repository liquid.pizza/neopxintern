import led
import time
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class GrowMode(Mode):

    def loop(self, np = led.np):

        for c in range(lc["setup"]["segmentLength"]):
            for y in range(lc["setup"]["segmentLength"]):
                col = self._config["endCol"]
                if y < c:
                    col = self._config["startCol"]

                for x in range(lc["setup"]["numSegments"]):
                    np[led.calcIdx(x, y)] = led.dimColor(col)
                    if lc["settings"]["mode"] != "GROW":
                        return False
            
            np.write()
            time.sleep_ms(self._config["shiftDelayInMs"])
        time.sleep_ms(self._config["delayInMs"])

        self._config["startCol"], self._config["endCol"] = self._config["endCol"], self._config["startCol"]