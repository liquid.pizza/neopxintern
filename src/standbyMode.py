import led
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class StandbyMode(Mode):

    def enterMode(self):
        led.off(led.np)

    def loop(self):
        pass