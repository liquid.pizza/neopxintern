import ujson

# read/write helper
CONFIG_FILENAME = "config.json"

def readJson(filename = CONFIG_FILENAME):
    with open(filename) as f:
        print(filename, "read.")
        return ujson.load(f)

def writeJson(data, filename = CONFIG_FILENAME):
    with open(filename, "w") as f:
        ujson.dump(data, f)
        print(filename, "written.")

ledConfig = readJson()

configIsUpdated = False
lastMode = None
