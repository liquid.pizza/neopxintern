import led
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class PercentMode(Mode):

    def enterMode(self):

        sLen = dh.ledConfig["setup"]["segmentLength"] 
        idx = sLen * self._config["percent"] / 100 - 1

        for i in range(sLen):
            color = self._config["lowLight"]

            if i <= idx:
                color = self._config["highLight"]
            
            for c in range(dh.ledConfig["setup"]["numSegments"]):
                led.np[led.calcIdx(c, i)] = color

        led.np.write()