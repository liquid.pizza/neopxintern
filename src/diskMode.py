import led
import time
import dataHandler as dh
from dataHandler import ledConfig as lc

from mode import Mode

class DiskMode(Mode):

    def disk(
        self,
        np,
        hl = (50, 50, 50),
        ll = (0, 0, 0),
        delayInMs = 100,
        start = 0,
        stop = lc["setup"]["segmentLength"],
        step = 1) :
        
        sl = lc["setup"]["segmentLength"]
        sn = lc["setup"]["numSegments"]

        for c in range(start, stop, step):
            for x in range(sn):
                for y in range(sl):

                    if lc["settings"]["mode"] != "DISK":
                        return
                    
                    idx = led.calcIdx(x, y)

                    if y == c:
                        np[idx] = hl
                    else:
                        np[idx] = ll

            np.write()
            time.sleep_ms(delayInMs)

    def loop(self, np = led.np):

        hl = self._config["highLight"]
        ll = self._config["lowLight"]
        
        delayInMs = self._config["shiftDelayInMs"]

        self.disk(np, hl, ll, delayInMs)
        self.disk(np, hl, ll, delayInMs, lc["setup"]["segmentLength"] - 1, -1, -1)