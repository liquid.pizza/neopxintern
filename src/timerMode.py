import led
import dataHandler as dh
from dataHandler import ledConfig as lc
import time

from mode import Mode

class TimerMode(Mode):

    def enterMode(self):
        led.setAll(led.np, self._config["lowLight"])
        self._run = True

        self._cntRow = 0
        # time in ms
        timeInMs = self._config["time"] * 1000
        # percent per LED row
        percentPerLed = 100 / dh.ledConfig["setup"]["segmentLength"]

        # time in ms per LED
        self._sleepTimeInMs = timeInMs * percentPerLed / 100

        print("timeInMs", timeInMs)
        print("percentPerLed", percentPerLed)
        print("self._sleepTimeInMs", self._sleepTimeInMs)
    
    def loop(self):
        print("TIMER: row", self._cntRow)

        sLen = dh.ledConfig["setup"]["segmentLength"]

        # set all LEDs
        for i in range(sLen):
            color = self._config["lowLight"]

            if i <= self._cntRow:
                color = self._config["highLight"]
            
            for c in range(dh.ledConfig["setup"]["numSegments"]):
                led.np[led.calcIdx(c, i)] = color

        led.np.write()

        # increase row count for next loop
        self._cntRow += 1

        # check if the end is reached
        self._run = self._cntRow < sLen

        # non blocking wait
        timestamp = time.ticks_ms()

        while (time.ticks_diff(time.ticks_ms(), timestamp) < self._sleepTimeInMs):
            # exit this loop as soon as a new command is received
            if (not self._run):
                return