# Readme

## Flash *micropython* scripts

We use [ampy](https://pypi.org/project/adafruit-ampy/) to load scirpts and files to the *micropython* device.

### List files

`ampy -p <port> ls`

### Upload file

`ampy -p <port> put <file>`

### Download file

`ampy -p <port> get <file>`

This command only prints out the files content.
To save this output to a file pipe it to file:

`ampy -p <port> get <remoteFile> > <localFile>`

Example:

`ampy -p /dev/ttyUSB0 get main.py > dlMain.py`

## MQTT broker

We use [mosca](https://github.com/moscajs/mosca) as MQTT broker.

### Install *mosca*

`npm i -g mosca`

### Start *mosca*

We run *mosca* in a *screen* session.

First start a new *screen* session:

`screen -S mosca`

The parameter `-S mosca` names the new session `mosca`.
This makes refinding the session way easier.

Next start mosca:

`mosca -v`

The parameter `-v` enters *verbose mode*, which delivers more output.
